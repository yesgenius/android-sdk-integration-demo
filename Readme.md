# Setup

## Adding dependency to your project

As the first step, you will need to define our maven repo in your **build.gradle** file:
```gradle
allprojects {
    repositories {
        maven {
            url "http://fcb-maven.cloudapp.net/repository/releases/"
        }
    }
}
```
After adding our repo you need to include a following gradle dependency:
```gradle
compile 'com.forcecube:forcecube-sdk-prod:1.+'
```


# Usage

## Initialisation

Get **APP_DEV_KEY** and **APP_DEV_SECRET** from ForceCuBe. Contact us at so@forcecube.com.

At the start of the app,  you must configure the SDK with the following code:
```java
public class YourApp extends Application {

    private static final String APP_DEV_KEY = "YOUR DEV KEY";
    private static final String APP_DEV_SECRET = "YOUR DEV SECRET";

    @Override
    public void onCreate() {
        super.onCreate();

        SdkConfiguration.Builder builder = new SdkConfiguration.Builder();
        builder.setDevKey(APP_DEV_KEY).setSecretKey(APP_DEV_SECRET);
        ForceCube.configure(this, builder.create());
    }
}
```
This will start a service a SDK service, if its not started.

To use SDK, you must create `ForceCube.Manager` instance, which will connect and communicate with the service.

Every activity making use of the SDK, must get an `ForceCube.Manager` instance, which will connect and communicate with the service.

Declare a main listener for the SDK:
```java
private final ForceCuBe.SdkListener mForceCubeListener = new ForceCuBe.SdkListener() {

    @Override
    public void onSdkStatus(ForceCuBe.State state, ForceCuBe.FatalError fatalError) {
    ...
    }

    @Override
    public void onSdkRuntimeError(ForceCube.CriticalError criticalError) {
    ...
    }

    @Override
    public void onCampaignOfferDelivered(long campaignOfferId) {
    ...
    }
};
```

Get `ForceCuBe.Manager` instance bu calling `ForceCuBe.bind`.

For example, you can call it in `onResume`.
```java
    @Override
    protected void onResume() {
        super.onResume();
        
        mForceCubeManager = ForceCuBe.bind(this, mForceCubeListener);
    }
```

When SDK conection is no longer needed, call 'ForceCuBe.Manager.unbind'.

```java
    @Override
    protected void onPause() {
        if (mForceCubeManager != null) {
            
            mForceCubeManager.unbind();

            mForceCubeManager = null;
        }

        super.onPause();
    }
```

## SDK's threading 

You must use each `ForceCuBe.Manager` instance on only one thread. That is, if you create manager on a background thread, use it and unbind it on the same thread. Callbacks from all the managers are coming on the main thread.

## Handling state changes and errors

When an internal state machine of the SDK is switched, then `onSdkStatus` is invoked. 
```java
    @Override
    public void onSdkStatus(ForceCube.State state, ForceCube.FatalError fatalError) {
    }
```
When the the SDK works properly, it has `STARTED` state, also it can have `STARTED_WITH_GEOFENCING` state when Bluetooth is turned off.
 
### Fatal errors

If you have received  the state `STOPPED` , it means that SDK is not able to continue this time. You have to check `fatalError`, which can be one of the following:
 - *BLE_NOT_SUPPORTED*. The device, on which the SDK is running, does not support BLE.
 - *UNABLE_TO_INIT_ENGINE_NO_NETWORK*.  SDK was unable to init, but will retry when network is available
 - *UNEXPECTED_STATUS_CODE_ON_SERVER_RESPONS*. SDK was unable to init, because FORCECUBE servers send unexpected status codes (5xx, 4xx), to so@forcecube.com
 - *INCORRECT_JSON_ON_SERVER_RESPONSE*.  SDK was unable to init, because FORCECUBE servers send unparsable json, please report this ASAP to so@forcecube.com
 - *LOCATION_SERVICES_ARE_OFF*. The location services are not turned on with desired accuracy (medium or high accuracy required).
 
### Runtime errors

While the SDK is working, there can be runtime errors. You can catch them using callback:
```java
    @Override
    public void onSdkRuntimeError(ForceCube.CriticalError criticalError) {
    }
```
*criticalError* can be one of the following: 
- UNKNOWN_ERROR
- SERVER_IS_DOWN
- UNEXPECTED_STATUS_CODE
- WRONG_JSON_FORMAT
- SERVER_AUTH_FAILURE
- NOT_ENOGH_DISK_SPACE
- FILE_SYSTEM_ERROR

Please, report these errors to the FORCECUBE team.

## Offer delivery

Offer delivery can be triggered either by GPS-based geofencing, when Bluetooth is switched off, or by BLE-beacons, when Bluetooth is switched on.

To receive wake up call from the SDK you must set pendingIntent with `Manager.setNotificationIntent`. It can reset it or clear it up after it has been used.

Get offer id with
```
long campaignOfferId = intent.getLongExtra("EXTRA_CAMPAIGN_OFFER_ID")
```

Also, SDK will call `SdkListener.onCampaignOfferDelivered` every time offer delivery is triggered.

### Showing offer details

To show offer details use `getCampaignOffer`.

### Showing campaign locations

To get a list of locations for a campaign use `getCampaignOfferLocations`.

## Managing campaign offer status

If the offer was delivered, but was not opened it has `UNOPENED` status. When a user opens offer to view it fullscreen you must force him to either save it or decline it. Use `setCampaignOfferAsAccepted` and `setCampaignOfferAsDeclined`

## Getting a list of offers accepted by user

You should use `getAcceptedOffers` method to show a list of offers, accepted by the user. Every item in the collection is a `CampaignOffer` instance.

## Managing offer redemption process

To let user redeem a coupon, you must use `setDataWritingListener` and `setDataWritingContext` (with offer id) methods. 

Data writing process is started when a user touches USBBeacon with his/her smartphone. After that SDK, checks if `dataWritingContext` is set. If it is, SDK goes ahead, if its not, a process stops.

When `dataWritingContext` is set, SDK checks it for consistency. If the context is campaign offer id, then that offer should be in `ACCEPTED` state and the location in which a redemption is going on should be eligible for the campaign.

If the checks went through `onDataWritingStarted` is called, otherwise `onDataWritingFailedToStart` will be called. After these calls some activity indication could be shown to the user.

When data writing process finishes `onDataWritingFinished` is called. Check `error` for device communication error.  In the case of error a retry attempt can be done.

If the process finished successfully, call `setCampaignOfferAsRedeemed`.

If a user has redeemed the coupon, it will be removed from the `getAcceptedOffers` results list.

## Sending app's user ID to FORCECUBE

To match data that FORCECUBE has with app's user data, the app should send its user id, using `setExternalId` method