package com.forcecube.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.forcecube.sdk.ForceCuBe;
import com.forcecube.sdk.SdkConfiguration;

/**
 * Created by aaistom on 10/7/16.
 */
public final class StartAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            SdkConfiguration.Builder builder = new SdkConfiguration.Builder();
            builder.setDevKey(MainApp.APP_DEV_KEY)
                    .setSecretKey(MainApp.APP_DEV_SECRET);

            if (!ForceCuBe.isStarted(context))
                ForceCuBe.start(context, builder.create());
        }
    }
}