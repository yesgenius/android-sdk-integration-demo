package com.forcecube.demo;

import android.app.Application;

import com.forcecube.sdk.ForceCuBe;
import com.forcecube.sdk.SdkConfiguration;

/**
 * Created by aaistom on 10/7/16.
 */

public class MainApp extends Application {

    public static final String APP_DEV_KEY = "825aa8cb-2020-485f-9ff7-6384e35787a5";
    public static final String APP_DEV_SECRET = "test";

    @Override
    public void onCreate() {
        super.onCreate();

        SdkConfiguration.Builder builder = new SdkConfiguration.Builder();
        builder.setDevKey(APP_DEV_KEY).setSecretKey(APP_DEV_SECRET)
                .setExternalId(Utils.getLogMarker());

        ForceCuBe.start(this, builder.create());
    }
}