package com.forcecube.demo;

import android.os.Build;

/**
 * Created by aaistom on 11/24/16.
 */
public final class Utils {
    private static final int MARKER_LENGTH = 8;
    private static final char MARKER_CHAR = '0';

    private Utils() { /* empty */ }

    public static String bytesToHexString(byte[] bytes){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < bytes.length - 1; ++i){
            sb.append(String.format("%02x:", bytes[i] & 0xff));
        }

        sb.append(String.format("%02x", bytes[bytes.length - 1] & 0xff));
        return sb.toString();
    }

    public static String getLogMarker() {
        int serialLength = (Build.SERIAL != null) ? Build.SERIAL.length() : 0;

        StringBuilder stringBuilder = new StringBuilder(MARKER_LENGTH);
        for (int i = (MARKER_LENGTH - 1); i >= 0; --i) {
            if (i < serialLength)
                stringBuilder.append(Build.SERIAL.charAt(i));
            else
                stringBuilder.append(MARKER_CHAR);
        }

        return stringBuilder.toString().toUpperCase();
    }
}
